# Exercise 3

This is the third exercise of a three part test. It contains a python program which has a variety of functions, all of them explained in comentaries.

### Requirements

To properly execute the program it is required to have installed:

* Python 3.6 or above.


### Testing

Since this was asked to be definitions of various functions to be able to prove they work as expected I have developed a series of tests for each function. To execute the tests it is required to have installed:

* Pytest for python3

**Running the tests:**
```
python3 -m pytest
```