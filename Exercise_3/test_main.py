import main

def test_myLength():
    assert main.myLength([1, 2, 3]) == 3
    assert main.myLength(["a", 2, 3, "b"]) == 4
    assert main.myLength([]) == 0

def test_clean_list():
    assert main.clean_list(["a", 33, "asd", 1]) == [33, 1]
    assert main.clean_list(["a", "aa", "asd", "cc"]) == []
    assert main.clean_list([2, 76, 18]) == [2, 76, 18]
    assert main.clean_list([]) == []

def test_are_numbers():
    assert main.are_numbers([1, 2, 456, 98745, 5]) == True
    assert main.are_numbers([8, "a", 56, "test"]) == False
    assert main.are_numbers(["st", "s", "t"]) == False

def test_myMaximum():
    assert main.myMaximum([5, 63, 8, 879]) == 879
    assert main.myMaximum([75, "gh", 2, "te"]) == 75
    assert main.myMaximum(["a", "gh", "te"]) is None

def test_average():
    assert main.average([1, 2, 3, 4]) == 2.5
    assert main.average([23, 55]) == 39
    assert main.average([97]) == 97

def test_buildPalindrome():
    assert main.buildPalindrome([1, 2, 3]) == [3, 2, 1, 1, 2, 3]
    assert main.buildPalindrome(["A", "B", "C"]) == ["C", "B", "A", "A", "B", "C"]
    assert main.buildPalindrome(["a", 2]) == [2, "a", "a", 2]

def test_remove():
    assert main.remove([1, 2, 3, 4, 5], [2, 5, 1]) == [3, 4]
    assert main.remove([2, 2, 2, 2, 55, 899, "a", "test"], [2, 2, 55, "a"]) == [899, "test"]
    assert main.remove([1, 88, 88, 1], [88, 1]) == []

def test_flatten():
    assert main.flatten(["a", ["b", ["c"]]]) == ["a", "b", "c"]
    assert main.flatten([1, ["b", [33, ["f"]]]]) == [1, "b", 33, "f"]
    assert main.flatten([[2], [3, [[5]]]]) == [2, 3, 5]

def test_oddsNevens():
    assert main.oddsNevens([1, 2, 3, 4, 5, 6]) == ([2, 4, 6], [1, 3, 5])
    assert main.oddsNevens([88, 2]) == ([88, 2], [])
    assert main.oddsNevens([15, 12, 1, 32]) == ([12, 32], [15, 1])

def test_primeDivisors():
    assert main.primeDivisors(60) == [2, 2, 3, 5]
    assert main.primeDivisors(6578) == [2, 11, 13, 23]
    assert main.primeDivisors(2) == [2]

def test_is_increasing():
    assert main.is_increasing([123, 56789, 987, 2]) == [True, True, False, False]
    assert main.is_increasing([1]) == [False]
    assert main.is_increasing([1547, 4921, 2222, 159]) == [False, False, True, True]
