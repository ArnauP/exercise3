def myLength(L):
    """
    Returns the lenght of a given list.
    """
    return len(L)

def clean_list(L):
    """
    Removes non digits from a list.
    """
    return [x for x in L if str(x).isdigit()]

def are_numbers(L):
    """
    Returns True if all elements of a list are numbers.
    """
    if all(isinstance(item, int) for item in L) is not True:
        return False
    return True

def myMaximum(L):
    """
    Returns the maximim number in a list. Raises ValueError if its given an emtpy list.
    """
    cleaned_list = clean_list(L)
    if cleaned_list == []:
        return
    return max(cleaned_list)

def average(L):
    """
    Returns the average of a given list. Raises ZeroDivisionError if its given an empty list.
    """
    cleaned_list = clean_list(L)
    return sum(cleaned_list) / len(cleaned_list)

def buildPalindrome(L):
    """
    Returns a palindrome of the given list that starts with the reverse of the list.
    """
    result = L[::-1]
    result.extend(L)
    return result

def remove(L1, L2):
    """
    Returns L1 after removing the occurences of the elements in L2. 
    """
    return [x for x in L1 if x not in set(L2)]

def flatten(L):
    result = []
    for element in L:
        if isinstance(element, list):
            result += flatten(element)
        else:
            result.append(element)
    return result

def oddsNevens(L):
    """
    Returns a tuple of two lists the first one containing all even numbers sorted as given and the second one with all odd numbers sorted as given.
    """
    if not are_numbers(L):
        return

    even = []
    odd = []
    for item in L:
        if item % 2 == 0:
            even.append(item)
        else:
            odd.append(item)
    return even, odd

def primeDivisors(n):
    """
    Returns a list of all prime divisors of a positive non-zero number.
    """
    result = []
    while n % 2 == 0:
        result.append(2)
        n = n / 2
    for number in range(3, int(n) + 1):
        while n % number == 0:
            result.append(number)
            n = n / number
    return result

def is_increasing(L):
    '''
    Returns a list of booleans. For each number, returns True if the numebr is increasing and False otherwise.
    '''
    def check_number(number):
        if len(str(abs(number))) <= 1:
            return False

        ordered_number = ''.join(sorted(str(number)))
        return number == int(ordered_number)

    return list(map(check_number, L))
